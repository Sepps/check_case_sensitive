function (check_case_sensitive)
	set(prefix MAKE_FOLDER_GOOD)
	set(flags)
	set(singleValues TARGET)
	set(multiValues)

	# Parse TARGET, but also allow for more args later.
	cmake_parse_arguments(
		"${prefix}"
		"${flags}"
		"${singleValues}"
		"${multiValues}"
		${ARGN}
	)

	# Filter all non-dirs
	get_property(TARGET_SOURCE_DIR TARGET ${MAKE_FOLDER_GOOD_TARGET} PROPERTY SOURCE_DIR)
    file(GLOB_RECURSE ALL_DIRECTORIES LIST_DIRECTORIES true CONFIGURE_DEPENDS */)
	foreach(DIR_PATH ${ALL_DIRECTORIES})
		if (NOT IS_DIRECTORY ${DIR_PATH})
			list(REMOVE_ITEM ALL_DIRECTORIES ${DIR_PATH})
		endif()
	endforeach()

	# Make the dir case sensitve
	if (WIN32)
		foreach(DIR_PATH ${ALL_DIRECTORIES})
			execute_process(COMMAND fsutil.exe file queryCaseSensitiveInfo "${DIR_PATH}" OUTPUT_VARIABLE STDIO)
			message(CHECK_START "Check ${DIR_PATH}")
			if(${STDIO} MATCHES "disabled")
				message(CHECK_FAIL "not case sensitive")
			elseif(${STDIO} MATCHES "enabled")
				message(CHECK_PASS "case sensitive")
			else()
				message(CHECK_FAIL "unknown case sensitivity")
			endif()
		endforeach()
	endif()
endfunction()
