A simple cmake function used to check the case sensitivity of folders under a cmake target.

Just copy the `check_case_sensitive.cmake` file into your project and then:
```
include(cmake/check_case_sensitive.cmake)
add_library(lib)
check_case_sensitivity(TARGET lib)
```
